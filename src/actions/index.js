import fetch from 'isomorphic-fetch';

export function fetchData() {
    return function(dispatch) {
        // dispatch({type: 'LOADING'});
        return fetch('http://localhost:3000/data')
            .then(resp => resp.json())
            .then(data => dispatch({type: 'FETCH_DATA', payload: data}));
    }
}

export function deleteData(id) {
    return function (dispatch) {
        return fetch(`http://localhost:3000/data/${id}`, {
            method: 'DELETE'
        })
            .then(resp => dispatch({type: 'DELETE_DATA', id: parseInt(id)}));
    }
}