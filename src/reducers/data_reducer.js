export default function dataReducer(state = [], action) {
    switch (action.type) {
        case 'FETCH_DATA':
            return action.payload;
        case 'DELETE_DATA':
            return state.filter(data => data.id !== action.id);
        default:
            return state;
    }
}