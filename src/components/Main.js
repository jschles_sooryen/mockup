import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as actions from '../actions';
import AppHeader from './AppHeader';
import AppMessage from './AppMessage';
import AppContent from './AppContent';
import AppButton from './AppButton';

class Main extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: "Andy"
        }
    }

    componentDidMount() {
        this.props.actions.fetchData();
    }

    handleDelete = (event) => {
        const id =  event.target.id;
        this.props.actions.deleteData(id);
    };

    render() {
        // Takes '/' off of path and applies it as main class of container div
        const urlClass = this.props.history.location.pathname.slice(1);
        return (
            <div className={urlClass}>
                <AppHeader />
                <AppMessage
                    name={this.state.name}
                />
                <AppContent>
                    {this.props.data.map(button => {
                        return (
                            <AppButton
                                id={button.id}
                                key={button.name}
                                text={button.name}
                                icon={button.logo}
                                delete={this.handleDelete}
                            />
                        )
                    })}
                </AppContent>
            </div>
        );
    }
}

/*
{
  "data": [
    { "id": 1, "name": "Retirement", "logo": "watch" },
    { "id": 2, "name": "College Savings", "logo": "school" },
    { "id": 3, "name": "Major Purchase", "logo": "home" },
    { "id": 4, "name": "Other", "logo": "forward" }
    ]
    }
 */

const mapStateToProps = (state) => {
    return { data: state.data };
};

const mapDispatchToProps = (dispatch) => {
    return { actions: bindActionCreators(actions, dispatch) };
};

export default connect(mapStateToProps, mapDispatchToProps)(Main);