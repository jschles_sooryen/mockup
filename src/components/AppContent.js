import React, { Component } from 'react';

class AppContent extends Component {
    render() {
        return (
            <div className="content">
                <h5 className="directions">Select your primary investment goal:</h5>
                {this.props.children}
            </div>
        );
    }
}

export default AppContent;