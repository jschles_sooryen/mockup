import React from 'react';

const AppButton = (props) =>
    <button className="app-button">
        <div className="button-content" id={props.id} onClick={props.delete}>
            <i className="material-icons">{props.icon}</i>
            <h5>{props.text}</h5>
        </div>
    </button>

export default AppButton;