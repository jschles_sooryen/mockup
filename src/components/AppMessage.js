import React from 'react';

const AppMessage = (props) =>
    <div className="message">
        <h2>Welcome, {props.name}!</h2>
        <h2>Let's start with Your Investment Goal.</h2>
    </div>

export default AppMessage;